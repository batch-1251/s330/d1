const express=require('express');
const mongoose=require('mongoose')

const app=express();
const PORT=5000;

//from routes
let tasksRoutes=require('./routes/tasksRoutes.js');

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://louie011:lowiidr011@cluster0.7ajhf.mongodb.net/s31?retryWrites=true&w=majority",
				{
					useNewUrlParser:true,
					useUnifiedTopology:true
				}
	).then(()=>console.log(`connected to Database`)).catch((error)=>console.log(error));

app.use("/",tasksRoutes);

app.listen(PORT, ()=>console.log(`Server running on port ${PORT}`));