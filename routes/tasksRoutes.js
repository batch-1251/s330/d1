const express=require('express');
const router=express.Router();

//from models
let User=require('./../models/User.js');

//from controllers
let userControllers=require('./../controllers/userControllers')

//insert a new task
router.post("/add-task",(req,res)=>{
	// console.log(req.body);
	userControllers.createUser(req.body).then(result=>res.send(result))


})

//retrieve all user
router.get("/users", (req,res)=>{
	//model.method-search users from the database
		//send it as a response
		// User.find({},(result,error)=>{
		// 	if(error){res.send(error)}
		// 		else{res.send("Users:",result)}
		// })
		userControllers.retrieveAllUser().then(result=>res.send(result))
})
//retrieve specific user
router.get("/users/:id",(req,res)=>{
	// console.log(req.params);
	let params=req.params.id
	// User.findById(params,(result,error)=>{
	// 	if(error){res.send(error)}
	// 		else{res.send("User:",result)}
	// })
	userControllers.retrieveUser(params).then(result=>res.send(result))
})
//update a user's info
router.put('/users/:id',(req,res)=>{
	//model.method
	//send update as response
	let params=req.params.id
	// User.findByIdAndUpdate(params,req.body,{new:true},(result,error)=>{
	// 	if(error){res.send(error)}
	// 		else{res.send(result)}
	// })
	userControllers.updateUser(params, req.body).then(result=>res.send(result))
})
//delete user
router.delete('/users/:id',(req,res)=>{
	let params=req.params.id
	// User.findByIdAndDelete(params,(error,result)=>{
	// 	if(error){res.send(error)}
	// 		else{res.send(true)}
	// })
	userControllers.deleteUser(params).then(result=>res.send(true))
})
module.exports=router;