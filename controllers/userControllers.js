//insert a new user
// module.exports.createUser
const User=require('./../models/User.js')
module.exports.createUser=(reqbody)=>{
	let newUser=new User({
			firstName:reqBody.firstName,
			lastName:reqBody.lastName,
			userName:reqBody.userName,
			passWord:reqBody.passWord
	})
	return newUser.save().then((error,savedUser)=>{
		if(error){ return error}
			else{return savedUser}
	})
}
// createUser(req.body)

module.exports.retrieveAllUser=()=>{

	return User.find().then((result,error)=>{
		if(error){return error}
			else{return result}
	})
}

module.exports.retrieveUser=(params)=>{
	return User.findById(params).then((error, result)=>{
		if(error){return error}
			else{return result}
	})
}



module.exports.updateUser=(params, reqbody)=>{
	
	return User.findByIdAndUpdate(params,reqbody,{new:true}).then((result,error)=>{
		if(error){return error}
			else{return result}
	})
}

module.exports.deleteUser=(params)=>{
	return User.findByIdAndDelete(params).then((result,error)=>{
		if(error){return error}
			else{return result}
	})
}